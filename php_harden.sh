##############################################
# Ubuntu Hardening Script for Apache mod_php #
#     THIS IS FOR FRESH INSTALLS ONLY        #
#       11/23/2015 - Dustin Larmeir          #
#       Tested on Ubuntu 14.04 LTS           #
##############################################


###################
# PHP5  Hardening #
###################

# Start Script
echo "*** Starting PHP Hardening Script ***"
echo ""
date
cat /etc/lsb-release
php -v
sleep 2
echo ""

# Disable Expose_PHP
echo "*** Disabling Expose PHP ***"
expose_php=off
sed -i 's/expose_php = .*/expose_php = '${expose_php}'/' /etc/php5/apache2/php.ini
sleep 1
echo "Done"
echo ""
