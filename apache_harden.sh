#!/bin/bash
##############################################
# Ubuntu Hardening Script for Apache Servers #
#     THIS IS FOR FRESH INSTALLS ONLY        #
#       11/21/2015 - Dustin Larmeir          #
#       Tested on Ubuntu 14.04 LTS           #
##############################################


####################
# Apache Hardening #
####################

# Start Script
echo "*** Starting Hardening Script ***"
echo ""
date
cat /etc/lsb-release 
apache2ctl -v
sleep 2
echo ""

# Disable Directory Index Module
echo "*** Disabling Directory Indexing ***"
a2dismod autoindex
sleep 1
echo ""

# Disable Apache Status Module
echo "*** Disabling Mod_Status ***"
a2dismod status
sleep 1
echo ""

# Getting rid of nasty and revealing Server Tokens
echo "*** Adjusting ServerTokens to Prod ***"
ServerTokens=Prod
sed -i 's/ServerTokens .*/ServerTokens '${ServerTokens}'/' /etc/apache2/conf-enabled/security.conf 
sleep 1
echo "Done"
echo ""

# Getting rid of nasty and revealing Server Signatures
echo "*** Removing Server Signatures ***"
ServerSignature=Off
sleep 1
sed -i 's/ServerSignature .*/ServerSignature '${ServerSignature}'/' /etc/apache2/conf-enabled/security.conf
echo "Done"
echo ""

# Disable Trace Enable
echo "*** Ensuring Trace Enable = Off ***"
TraceEnable=Off
sleep 1
sed -i 's/TraceEnable .*/TraceEnable '${TraceEnable}'/' /etc/apache2/conf-enabled/security.conf
echo "Done"
echo ""

# Remove Default Site Files
echo "*** Removing Default Distro Index.html ***"
defaultpage=$(cat /var/www/html/index.html | grep -i "Apache2 Ubuntu Default Page" | tail -n1)
pagexists=$(echo "$defaultpage" | awk '{print $3}')
   if [ "$pagexists" = "Default" ]; then
      rm /var/www/html/index.html
   else
      echo "Default Page Already Removed"
   fi
sleep 1
echo "Done"
echo ""

# Restart Apache
service apache2 restart
#lsof -i :80 | awk '{print $1,$9,$10}' | uniq
echo ""
echo "*** Hardening Completed ***"
echo "If you notice any issues with this script please email dustin@larmeir.com"

#rm apache_harden.sh
########################
# End Apache Hardening #
########################
